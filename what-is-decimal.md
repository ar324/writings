# What Is a "Decimal" Anyway?

**May 26, 2022**

We are guilty of overloading the term "decimal". In its truest sense, "decimal"
is used in the context of the decimal numeral system (other numeral systems
include the binary and hexadecimal numeral systems). However, it seems like we
today refer to any non-integer number as a "decimal" (noun), or a "decimal
number". In this sense, 1.44 is called a decimal. Consider the number 3.14: we
also refer to the "point" as "decimal". In addition, we can also refer to the
numbers after the point, 1 and 4, as decimals: "3.14 has two decimals".

For those who do not know the difference between numerals and numbers: a numeral
is a symbol that represents a number. The same number might have multiple
representations in different numeral systems: the number represented by the
decimal numeral 12, for instance, is represented as "30" in the quaternary
numeral system, and as "14" in the octal numeral system.

Let's also talk about separators. In any numeral system, we make use of
separators to, well, separate the integer part from the fractional part. In the
decimal system, we most commonly make use of the "decimal point" as the
separator. We could also use the "decimal comma". However, these two terms do
not (and should not) apply to other numeral systems. In general, a separator in
any numeral system can be called "radix point", or "radix character". In the
fractional binary numeral 10.11 (2.75 in decimal representation), a dot is used
as the radix point.

25 is as much decimal as 25.01, even though it has no fractional part--both are
decimal numerals. I think we should do away with decimal's connotation of
representing a non-integer. It does not work with other bases (numeral systems);
moreover "non-integer" just makes much more sense--why should we use the name of
a numeral system to indicate a non-integer?

Ideally, I would also have us drop the use of "decimal" to refer to the "decimal
point" as well. Again, it makes little sense to overload the term. "Decimal
point" is far more clear, and can be extended to other numeral systems in a
similar fashion: "octal point", "binary point", et cetera.

Referring to the numbers after the decimal point as "decimals", however, is
perhaps our worst sin. There is no reason why the term should only refer to the
numbers after the decimal point: would we call the two 1s in the binary numeral
10.11 "binaries"? That would not make sense! Instead, we could call them
"fractional digits". 3.14 would then have two fractional digits. This is far
more clear, in my opinion--moreover, it is applicable to all numeral systems.
