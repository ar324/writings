# DNF Is the Most Vexatious Software I Have Ever Used

**June 26, 2022**

(Warning: I have not read DNF's manual thoroughly, so I might be to blame here.)

Fedora is the Linux distribution of my choice. I have no complaints against it
most of the time—except when it comes to DNF. I have used several package
managers over the years—APT, Pacman, XBPS, and APK—but none of them has vexed me
as much as DNF.  Let me go ahead and say it: DNF is the most vexatious software
I use on a daily basis. This short article will delineate why I feel so.

Every package manager I've used in the past has never coupled package
installation with metadata-updating: when I run `apt install package-name` or
`pacman -S package-name`, for example, APT and Pacman will work with the
existing repository metadata on my system. However, DNF is several cuts above
its peers: DNF will also update your repository metadata each time you try to
install a package. Is that good software design? My intuition tells me it is
manifestly not—it seems to violate the inviolable principle of modularity. Even
then, I likely would not have written this, had metadata updates been snappy.
Unfortunately, I do not find myself so lucky. Let us have a look at how slowly
DNF updates its metadata cache:

[dnf-update-cache.mkv](videos/dnf-update-cache.mkv)

It took about 10 seconds to begin the main installation process. To be sure, you
do not always need to have the latest repository metadata to install a package:
it is sometimes quite alright to work with old metadata. However, DNF does not
think so. Every install must be accompanied by a 10-second–long metadata update.
Now, to be fair, DNF will not update your metadata if there has been a "recent"
update: I think metadata expire after 24 hours. Here too, however, DNF is dozens
of cuts above its peers. Let us have a look at why this is so:

[dnf-check-cache.mkv](videos/dnf-check-cache.mkv)

Did you notice the 1-second delay between the beginning of the command and the
printing of "Last metadata check: . . ."? Let it sink in: DNF takes one whole
second, on a modern computer, to check if it needs to update its repository
metadata cache. To those of you who have never used DNF, this 1-second delay
might seem like a trifle, but I must have run DNF thousands of times by now to
realize how inefficient it is. APT and Pacman, for example, are almost
instantaneous (give it a try, they "truly" are)—they work with the system's
existing cache.

DNF has a '-C' option, which the manual describes in the following way: "Run
entirely from system cache, don't update the cache and use it even in case it is
expired." Let us take that for a spin after updating our system cache, shall we?
Here is the (unfortunate) result:

[dnf-from-cache.mkv](videos/dnf-from-cache.mkv)

That is right—DNF is trying to install packages from the system's metadata
cache, which begs the question: Why would DNF try to install a package from the
"metadata" cache? . . . Unless, it's not a metadata cache: maybe it is a generic
cache, in which case, it begs another, perhaps even more important, question: Is
there an option to use just the metadata part of the generic cache, and download
the package from package mirrors instead? No, there is not. DNF users are to
suffer eternally with 1-second—long predicate functions.

I have only scratched the surface so far. In one of the foregoing paragraphs, I
said that DNF will update (or check) your metadata cache every time you try to
install a package: I left out the fact that DNF will do so for "any"
operation—even search. Also, DNF maintains different metadata caches for each
user. There are countless other things that are wrong with DNF, which I will
leave for you to discover. Until then, goodbye; I am going to update my system
with a heavy sigh.
