This folder contains first drafts of some of my writings. Granted, even though I
likely will not share drafts that will bring shame to my writing abilities, I
shall try to share as many of my drafts as possible—let them serve as a memento of
how things can always be made better!
