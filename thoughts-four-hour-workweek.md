# My Thoughts Going into The 4-Hour Workweek

**July 19, 2022**

A few pages into The 4-Hour Workweek by Tim Ferriss, I was struck by what Tim
claimed to be most people's "plan": work, save money, and "retire" at 62. I have
never thought of life in that manner. I will grant that I do not have a fully
fleshed out life plan yet; retirement, however, seems a strange concept to me.
In Tim's book, what would this average person do after retirement—relax and
engage in merriment round the clock? Relaxation can beget happiness, but in my
experience, lasting happiness only comes from creation of things palpable and
intellectually stimulating such as writings, code, or podcasts.

Tim has made a clear distinction between actions you perform to keep yourself
engaged and the actions you perform to make money to sustain yourself. The
latter, I presume, is the "4-hour workweek" he refers to. Tim Ferriss apparently
advocates minimizing the amount of time one spends on money-making actions while
maximizing their impact. However, to me, this dichotomy does not exist. What I
have come to like the most—computer science—is as well what generates my income.
Thus, I do not seek to minimize the time I spend on it. I have other interests
as well, but I also have ample time to pursue them. What truly underlies the
success of my pursuing of the aforementioned interests is my own time management
skills. Cal Newport, a popular computer scientist and the man behind the term
"deep work", comes to mind as another example: he makes money doing what he
loves. For Cal too Tim's dichotomy does not exist. Therefore, I infer that Tim
is specifically targeting those whose interests have little potency to generate
a living. I am thinking of engagements like skiing, hiking, and running.

I sometimes conduct the following thought experiment: What if everybody in the
world became an "influencer"? Influencers do not have any value in and of
themselves: instead, they persuade those valuable to undertake an action or make
a purchase. Thus, if everybody were influencers, there would be no value to
capitalize on and the world would collapse. I do not believe "easy money" is the
right way to earn a living: there must be creation of value for happiness—I have
scientists, engineers, creativists, and designers in mind.

There seems to be another motif in Tim's book that resonates with me far
more—the idea of lifestyle design. Lifestyle design confronts you with a
fundamental question: How do you want to spend your time? The thought that goes
into answering this question puts occurrences, challenges, and actions in the
right, long-term perspective. In reading the book, this is the thread I aim to
explore further. I always had the concept underlying lifestyle design in my
mind, although putting words to it has made it far easier to think about and
expand on.
