# On Passivity and Nothingness

**November 14, 2021**

One popular form of meditation is Vipasana. In Vipasana, you observe the true
nature of your mind. You realize that thoughts that appear to be your own
creation are not in fact the products of your making. Upon examination, you see
that your thoughts arise from nowhere, as a matter of experience. On attaining
this knowledge, you then rest as the space in which thoughts appear—noticing
them as they are, without judgement. Judgement too is a thought that can be
observed.

Vipasana's objective, I think, is to break identification with spontaneous
thoughts. Does Vipasana seek to make you identify with the 'space-like'
container that everything else arises in? I do not think so. It is very likely
that Vipasana does not want you to identify with anything at all. It merely
wants you to notice the true nature of the way things are. This noticing
alleviates much suffering and pain.

The only other form of meditation I know of is what I call 'concentration
meditation'. Here, you focus on an object of your picking with intent. Some
people focus on their breath, and some on different number series. The goal here
is apparent―to enhance your ability to focus. Concentration meditation also
helps alleviate suffering, because pain is not noticed as much when focusing
intently on something else.

Vipasana is evidently more transcendental. It also arguably is not necessary to
live a happy life. You do not necessarily need to know the true nature of your
self to be happy. It is true that a case can be made that you would be happier
with Vipasana. That however is not my point.

Many of us feel relief while mindlessly watching the television. I have been
trying to understand what happens to us in that state of mind. Adults do not
need to exert much effort to understand speech (and a rapid flash of pictures).
You need only notice the automaticity with which your mind comprehends speech to
realize this. If you're watching content that needs little thought for
near-total comprehension (like a music video), your mind is effectively relaxed
in a state of nothingness. It is stimulated, although the effort that goes into
this stimulation comes from outside. This stimulation allows you to momentarily
forget everything else without expending much energy.

Contrast this with reading a technical book. You need to 'generate' to gain
comprehension. At every moment, you need to sustain packets of information in
your head to make sense of what comes ahead. If you lose any of these packets,
you must actively recall it. All this takes effort.

A question I have been asking myself as of late is whether 'passive activities'
are at all necessary in our lives. There are different reasons why we engage in
passivity:

* If I just spent an hour intently focussing on a mathematical problem I've been
  trying to solve, I am bound to be tired. Passivity can help me forget the
  feeling of being tired.
* Some people have negative and demeaning inner voices. Passivity quells that
  voice.
* Passivity can help silence the Guilt of Procrastination.

All three cases above have escapism in common. Let us assume for a minute that
instead of 'passiving', you simply sat alone with your thoughts. Unless you are
a Vipasana expert, you would almost certainly have to expend more effort by
simply sitting (thoughts consume energy). This is however only during the 'rest
period'. In my experience, you feel truly rested afterward. This is because you
are forced to process or at least acknowledge your thoughts. Passiving on the
other hand will make you feel significantly worse afterward. This is because
what you're trying to avoid is always present in your mind. Passiving only
constructs a thin wall that breaks down easily. There is no true rest.

Hence I do not think passivity is a necessary component in our lives. That which
is not necessary however should not necessarily be jettisoned. It however does
have detrimental effects. Passivity encourages further passivity owing to your
not feeling rested and the resurgence of thoughts in your mind after the passive
period. The chronic scroller exemplifies this. The chronic scroller is in
constant search for true rest. They mistake the illusion of rest of passivity as
true rest and get worse. The cycle thus continues.

I do not think passivity should have a place in my life. I must learn to
appreciate the beauty of life as it is―escapism should not ideally be necessary.
As time goes on, I think tolerating the vacuity or lack there of of your mind
becomes deeply comforting, and more importantly, restful.
